# Some practical links:

### Kotlin:
- http://lprovot.fr/Android/Kotlin.html
- https://openclassrooms.com/fr/courses/5353106-initiez-vous-a-kotlin
- https://try.kotlinlang.org (To try Kotlin)

### Git:
- https://www.git-scm.com/book/ (Mr PROVOT's link)
- http://marc.chevaldonne.free.fr/ens_rech/DocumentsDivers_files/Quick%20Guide%20Git%20TortoiseGit.pdf
- https://carlchenet.com/category/debuter-avec-git/
- https://openclassrooms.com/fr/courses/1233741-gerez-vos-codes-source-avec-git
- Markdown (.md): https://docs.gitlab.com/ee/user/markdown.html

### Android (Here in Java, but there are ideas):
- https://openclassrooms.com/fr/courses/4517166-developpez-votre-premiere-application-android
- https://openclassrooms.com/fr/courses/3499366-developpez-une-application-pour-android
- https://openclassrooms.com/fr/courses/2023346-creez-des-applications-pour-android
- https://www.youtube.com/playlist?list=PLMS9Cy4Enq5JnwAxe6Ao74qSTxxXjiw7N

### Agile method
#### - SCRUM
- https://lprovot.fr/ScrumTranchees.pdf (Mr Provot's link)
- https://openclassrooms.com/fr/courses/4511226-gerez-votre-projet-avec-une-equipe-scrum/4511233-maitrisez-les-piliers-scrum

##### -- Official SCRUM guide
- https://www.scrumguides.org/docs/scrumguide/v2017/2017-Scrum-Guide-French.pdf (French)
- https://www.scrumguides.org/docs/scrumguide/v2017/2017-Scrum-Guide-US.pdf (English)

#### - Agile
- https://openclassrooms.com/fr/courses/4511316-perfectionnez-votre-gestion-de-projet-agile/4511323-preparez-le-projet-avec-la-methode-rad
- https://openclassrooms.com/fr/courses/4507926-initiez-vous-a-la-gestion-de-projet-agile/4507933-formez-votre-equipe

### Peertube API
- https://docs.joinpeertube.org/api-rest-reference.html
- https://instances.joinpeertube.org/api/v1/instances (obtenir les instances)

### Peertube Doc
- https://docs.joinpeertube.org/#/

#### - Queries
- https://square.github.io/retrofit/
- https://github.com/square/moshi/

### Normes et conseils d'ergonomie
- https://material.io/
