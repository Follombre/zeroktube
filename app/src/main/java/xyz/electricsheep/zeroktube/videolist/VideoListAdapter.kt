package xyz.electricsheep.zeroktube.videolist

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import xyz.electricsheep.zeroktube.BuildConfig
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.request.video.Video


/**
 *
 */

class VideoListAdapter(private var videoSet: List<Video>, val clickListener: (Video) -> Unit) :
    RecyclerView.Adapter<VideoListAdapter.VideoListViewHolder>() {

    /** Provides a reference to the views for each data item */
    class VideoListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val title: TextView = itemView.findViewById(R.id.title)
        private val thumbnail: ImageView = itemView.findViewById(R.id.thumbnail)

        /**
         * Binds a [Video] to a [RecyclerView] item,
         * a click event handler and loads the [thumbnail]
         * from [video]
         */

        fun bindVideo(video: Video, clickListener: (Video) -> Unit) {
            title.text = video.name
            itemView.setOnClickListener { clickListener(video) }
            if(BuildConfig.DEBUG) {
                Log.d("VideoListAdapter", "bindVideo thumbnailPath => " + video.thumbnailPath)
            }
            Picasso.get().load(video.thumbnailPath).into(thumbnail)
        }
    }

    /** Creates new views (invoked by the layout manager)*/
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): VideoListViewHolder {

        /** creates a new view */
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.videolist_item, parent, false)
        return VideoListViewHolder(view)
    }

    /** Replaces the content of a view */
    override fun onBindViewHolder(holder: VideoListViewHolder, position: Int) {
        /**
         * gets element from videoSet at this position and
         * replaces the content of the view with that element
        */
        holder.bindVideo(videoSet[position],clickListener)
    }

    /** Returns the size of [videoSet] (invoked by the layout manager) */
    override fun getItemCount() = videoSet.size

}