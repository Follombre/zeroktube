package xyz.electricsheep.zeroktube.videolist

import xyz.electricsheep.zeroktube.request.video.Video

interface AbstractVideoListActivity {
    fun onVideoListLoaded(list: List<Video>)
}
