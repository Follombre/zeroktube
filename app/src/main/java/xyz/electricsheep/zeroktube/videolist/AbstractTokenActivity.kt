package xyz.electricsheep.zeroktube.videolist

import xyz.electricsheep.zeroktube.request.authentication.UserToken

interface AbstractTokenActivity {
    fun onTokenLoaded(token: UserToken)
}