package xyz.electricsheep.zeroktube.videolist

import android.content.Context
import xyz.electricsheep.zeroktube.request.video.Description
import xyz.electricsheep.zeroktube.request.video.VideoIdRequest

interface AbstractVideoActivity {
    fun onVideoLoaded(video: VideoIdRequest, description: Description)
}
