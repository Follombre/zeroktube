package xyz.electricsheep.zeroktube.choose_instance.instanceList

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.widget.ArrayAdapter
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.choose_instance.instance.Instance
import xyz.electricsheep.zeroktube.choose_instance.RequestInstance
import xyz.electricsheep.zeroktube.view.VideoListActivity

class InstanceListActivity : AppCompatActivity(),
        AbstractInstanceActivity,
        AbstractInstanceListActivity {

    private lateinit var recycle: RecyclerView
    private lateinit var recyAdapter: RecyclerView.Adapter<InstanceListAdapter.InstanceListViewHolder>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var arrayAdapter : ArrayAdapter<Instance>
    private lateinit var totalInst : TextView
    private val requInst = RequestInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        requInst.getInstances(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.instancelist)
    }

    override fun onInstanceListLoaded(list: List<Instance>) {
        viewManager = LinearLayoutManager(this)

        arrayAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,list)

        recyAdapter = InstanceListAdapter(listInst = list, clickListener = { inst: Instance -> onInstanceLoaded(inst) })

        //initializes the recyclerView
        recycle = findViewById<RecyclerView>(R.id.recyclerInstance).apply {

            setHasFixedSize(true)
            adapter = recyAdapter
            layoutManager = viewManager

        }

        totalInst = findViewById(R.id.totalInst)
        totalInst.text = "Nombre d'instances Total : " + list.size.toString()
    }

    override fun onInstanceLoaded(inst: Instance) {
        var intent = Intent(this, VideoListActivity()::class.java)
        intent.putExtra("host", inst.host)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val menuItem = menu.findItem(R.id.search_icon)
        val searchView = menuItem.actionView as SearchView
        searchView.queryHint = "Search"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String): Boolean {
                if (query == ""){
                    requInst.getInstances(this@InstanceListActivity)
                }
                else{
                    requInst.searchInstance(query,this@InstanceListActivity)
                }

                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                if (query == ""){
                    requInst.getInstances(this@InstanceListActivity)
                }
                else{
                    requInst.searchInstance(query,this@InstanceListActivity)
                }
                return false
            }
        })

        return true
    }
}

