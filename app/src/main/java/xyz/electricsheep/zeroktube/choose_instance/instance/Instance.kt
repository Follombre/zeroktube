package xyz.electricsheep.zeroktube.choose_instance.instance

class Instance(var id: Int, var host: String, val name: String, var shortDescription: String, val lang: String)