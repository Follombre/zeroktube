package xyz.electricsheep.zeroktube.choose_instance.instance

class InstanceListRequest(val total: Int, val data: List<Instance> )