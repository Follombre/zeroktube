package xyz.electricsheep.zeroktube.choose_instance.instance

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ListInstanceService {
    @GET("instances")
    fun listInstance(@Query("start") start: Int,@Query("count") count: Int, @Query("healthy") health : Boolean): Call<InstanceListRequest>
}