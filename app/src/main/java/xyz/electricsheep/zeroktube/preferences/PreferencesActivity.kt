package xyz.electricsheep.zeroktube.preferences


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import xyz.electricsheep.zeroktube.R

//main sharedPreference= prefUser
class PreferencesActivity: AppCompatActivity()  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.preferences)
        /*
"displayNSFW": true,
"autoPlayVideo": true,
"displayName": "string",
"description": "string",
         */
        val pref = getSharedPreferences("prefUser",0)
        val  displayNSFW = findViewById<com.google.android.material.chip.Chip>(R.id.displayNSFW)
        val  autoPlayVideo = findViewById<com.google.android.material.chip.Chip>(R.id.autoPlayVideo )
        val  displayName = findViewById<com.google.android.material.textfield.TextInputEditText>(R.id.displayName)
        val  description = findViewById<com.google.android.material.textfield.TextInputEditText>(R.id.description)
        val  confirmButton = findViewById<Button>(R.id.confirmButton)
        displayNSFW.setChecked(pref.getBoolean("displayNSFW",false))
        autoPlayVideo.setChecked(pref.getBoolean("autoPLayVideo",false))
        displayName.setText(pref.getString("displayName",""))
        description.setText(pref.getString("description",""))
        val prefEditeur = pref.edit()
        displayNSFW.setOnClickListener {   prefEditeur.putBoolean("displayNSFW",!pref.getBoolean("displayNSFW",false))}
        autoPlayVideo.setOnClickListener {   prefEditeur.putBoolean("autoPlayVideo",!pref.getBoolean("autoPlayVideo",false))}
        displayName.setOnClickListener {   prefEditeur.putString("displayName",displayName.text.toString())}
        description.setOnClickListener {   prefEditeur.putString("description",description.text.toString())}
        confirmButton.setOnClickListener{prefEditeur.commit()}



    }
}