package xyz.electricsheep.zeroktube.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.navigation.NavController
import androidx.navigation.Navigation
import xyz.electricsheep.zeroktube.R

class SubscriptionsFragment: Fragment() {
    @Nullable
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_subscriptions, container, false)
}