package xyz.electricsheep.zeroktube.view.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import xyz.electricsheep.zeroktube.BuildConfig
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.request.video.VideoIdRequest


class VideoQualitiesFragment : BottomSheetDialogFragment() {
    private lateinit var ctx: Context

    private lateinit var video: VideoIdRequest

    fun newInstance(video: VideoIdRequest): VideoQualitiesFragment {
        this.video = video
        return this
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_video_qualities_popup_menu, container, false)

        val menu: LinearLayout = view.findViewById(R.id.video_qualities_popup)

        if(BuildConfig.DEBUG) {
            Log.d("VideoQualitiesGaethan", "Nb qualities => " + video.files.size.toString())
        }

        for (file in video.files) {

            if(BuildConfig.DEBUG) {
                Log.d("VideoQualitiesGaethan", "Quality label => " + file.resolution.label)
            }

            val quality: LinearLayout = inflater.inflate(R.layout.row_popup_menu, null) as LinearLayout
            val iconQuality = quality.findViewById<ImageView>(R.id.icon)
//            iconQuality.id = file.resolution.id
            iconQuality.setImageResource(R.drawable.ic_check_white)
            iconQuality.visibility = View.INVISIBLE
            val textQuality = quality.findViewById<TextView>(R.id.text)
            textQuality.text = file.resolution.label

            quality.setOnClickListener {
//                for (fileT in video.files) {
//                    val iconQuality = view.findViewById<ImageView>(fileT.resolution.id)
//                    iconQuality.visibility = View.INVISIBLE
//                }

                iconQuality.visibility = View.VISIBLE
            }

            menu.addView(quality)
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        if (context != null) {
            ctx = context!!
        }

        super.onActivityCreated(savedInstanceState)
    }
}