package xyz.electricsheep.zeroktube.view.fragment

import android.content.Context
import android.content.Intent
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import kotlinx.coroutines.*
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.request.video.Description
import xyz.electricsheep.zeroktube.request.video.VideoFiles
import xyz.electricsheep.zeroktube.request.video.ZerokDatabase
import xyz.electricsheep.zeroktube.request.video.VideoIdRequest
import kotlin.coroutines.CoroutineContext


class VideoInfoFragment : Fragment(), CoroutineScope by MainScope() {
    private lateinit var ctx: Context

    private var videoId: Int = 0
    private lateinit var zerokDatabase: ZerokDatabase
    private lateinit var video: VideoIdRequest
    private lateinit var description: Description
    private lateinit var video1 : Deferred<VideoIdRequest>
    private lateinit var files : Deferred<List<VideoFiles>>
    private lateinit var description1 : Deferred<Description>

    private lateinit var showDescription: ConstraintLayout
    private lateinit var title: TextView
    private lateinit var views: TextView
    private lateinit var dropDescription: ImageView

    private lateinit var likeButton: ConstraintLayout
    private lateinit var nbLikes: TextView
    private lateinit var dislikeButton: ConstraintLayout
    private lateinit var nbDislikes: TextView
    private lateinit var shareButton: ConstraintLayout
    private lateinit var downloadButton: ConstraintLayout
    private lateinit var saveButton: ConstraintLayout
    private lateinit var reportButton: ConstraintLayout
    private lateinit var job : Job

    private lateinit var descriptionText: TextView
    private var descriptionVisibility: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_video_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        showDescription = view.findViewById(R.id.showDescription)
        title = view.findViewById(R.id.title)
        views = view.findViewById(R.id.views)
        dropDescription = view.findViewById(R.id.drop)

        likeButton = view.findViewById(R.id.likeButton)
        nbLikes = view.findViewById(R.id.nbLikes)
        dislikeButton = view.findViewById(R.id.dislikeButton)
        nbDislikes = view.findViewById(R.id.nbDislikes)
        shareButton = view.findViewById(R.id.shareButton)
        downloadButton = view.findViewById(R.id.downloadButton)
        saveButton = view.findViewById(R.id.saveButton)
        reportButton = view.findViewById(R.id.reportButton)

        descriptionText = view.findViewById(R.id.description)

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        if (context != null) {
            ctx = context!!
        }
        zerokDatabase = ZerokDatabase.getInstance(ctx)

        job = GlobalScope.launch {
            video1 = async { zerokDatabase.videoIdDao().getVideoIdRequest(videoId) }
            files = async { zerokDatabase.filesDao().getAllVideoFiles(videoId) }
            description1 = async { zerokDatabase.descriptionDao().getDescription(videoId) }
            video = video1.await()
            video.files = files.await()
            description = description1.await()
        }
        runBlocking {
            job.join()
        }
        title.setText(video.name)
        views.setText(getString(R.string.views, video.views))

        nbLikes.setText(video.likes.toString())
        nbDislikes.setText(video.dislikes.toString())
        descriptionText.setText(description.description)
        descriptionText.visibility = View.GONE

        showDescription.setOnClickListener {
                if (descriptionVisibility) {
                    descriptionText.visibility = View.GONE //hide description
                    dropDescription.setImageResource(R.drawable.ic_arrow_drop_down_white)
                    descriptionVisibility = false
                } else {
                    descriptionText.visibility = View.VISIBLE //show description
                    dropDescription.setImageResource(R.drawable.ic_arrow_drop_up_white)
                    descriptionVisibility = true
                }
        }

        // When user click on the button to like
        likeButton.setOnClickListener {
            val text = "You must be logged in to like the video"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(ctx, text, duration)
            toast.show()
        }

        // When user click on the button to dislike
        dislikeButton.setOnClickListener {
            val text = "You must be logged in to dislike the video"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(ctx, text, duration)
            toast.show()
        }

        // When user click on the button to share
        shareButton.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, video.files[0].fileUrl)
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

        // When user click on the button to download
        downloadButton.setOnClickListener {
            /*val intent = Intent(ctx, DownLoadActivity()::class.java)
            startActivity(intent)*/
        }

        // When user click on the button to save
        saveButton.setOnClickListener {
            val text = "You must be logged in to save the video"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(ctx, text, duration)
            toast.show()
        }

        // When user click on the button to report
        reportButton.setOnClickListener {
            val text = "You must be logged in to report the video"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(ctx, text, duration)
            toast.show()
        }

        super.onActivityCreated(savedInstanceState)
    }

    fun setVideoId(id: Int) {
        videoId = id
    }
}