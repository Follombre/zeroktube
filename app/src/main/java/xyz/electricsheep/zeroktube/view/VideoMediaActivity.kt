package xyz.electricsheep.zeroktube.view

//import androidx.media2.player.MediaPlayer
import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentContainerView
import androidx.recyclerview.widget.RecyclerView
import xyz.electricsheep.zeroktube.BuildConfig
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.view.fragment.VideoInfoFragment
import xyz.electricsheep.zeroktube.view.fragment.VideoPlayerFragment


class VideoMediaActivity : AppCompatActivity() {
    private lateinit var videoPlayerFragment: VideoPlayerFragment
    private lateinit var videoInfoFragment: VideoInfoFragment

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreate(savedInstanceState: Bundle?) {
        if(BuildConfig.DEBUG) {
            Log.d("VideoMediaActivityGaeth", "onCreate")
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.video_media)

        // Get the videoplayer fragment
        videoPlayerFragment = supportFragmentManager.findFragmentById(R.id.videoPlayer) as VideoPlayerFragment
        videoPlayerFragment.setVideoId(intent.getIntExtra("id", 0))

        // Get the videoinfo fragment
        videoInfoFragment = supportFragmentManager.findFragmentById(R.id.videoInfo) as VideoInfoFragment
        videoInfoFragment.setVideoId(intent.getIntExtra("id", 0))

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        val videoInfo = findViewById<View>(R.id.videoInfo) as FragmentContainerView
        val recyclerComments = findViewById<View>(R.id.recyclerComments) as RecyclerView
        super.onConfigurationChanged(newConfig)
        if(BuildConfig.DEBUG) {
            Log.d("VideoListActivityGaeth", "onConfigurationChanged")
        }

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            hideSystemUI()
            videoInfo.visibility = View.GONE
            recyclerComments.visibility = View.GONE
        }
        else {
            showSystemUI()
            videoInfo.visibility = View.VISIBLE
            recyclerComments.visibility = View.VISIBLE
        }
    }

    private fun hideSystemUI() {
            window.decorView.systemUiVisibility = (
                    (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) View.SYSTEM_UI_FLAG_IMMERSIVE  else 0)
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    // Hide the nav bar and status bar
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }

    private fun showSystemUI() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }

    // When user click to "FullScreen"
    @SuppressLint("SourceLockedOrientationActivity")
    fun doFullScreen(view: View) {
        requestedOrientation = if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
//            ActivityInfo.SCREEN_ORIENTATION_SENSOR
        } else {
            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
    }
}