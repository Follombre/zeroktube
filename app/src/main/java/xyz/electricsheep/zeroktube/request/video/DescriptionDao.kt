package xyz.electricsheep.zeroktube.request.video

import androidx.room.*

@Dao
interface DescriptionDao {
    //Get all descriptions
    @Query("SELECT * FROM description")
    suspend fun getAllDescriptions() : List<Description>

    //Get a description based on the videoId it depends
    @Query("SELECT * FROM description WHERE videoId = :videoId")
    suspend fun getDescription(videoId : Int) : Description

    //Insert a description into the database
    @Query("INSERT INTO description (description, videoId) VALUES (:description ,:videoId)")
    suspend fun insertDescription(description: String?, videoId : Int)

    @Query("DELETE FROM description WHERE videoId = :videoId")
    suspend fun deleteDescFromVideoId(videoId: Int)
}