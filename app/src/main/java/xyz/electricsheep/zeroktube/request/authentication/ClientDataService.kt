package xyz.electricsheep.zeroktube.request.authentication

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ClientDataService {
    @GET("api/v1/oauth-clients/local")
    fun getData(): Call<ClientData>
}