package xyz.electricsheep.zeroktube.request.video

class Video(val id: Int, val desc: String?, val name: String, var thumbnailPath: String, val duration: Int)