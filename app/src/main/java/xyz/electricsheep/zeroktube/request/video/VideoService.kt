package xyz.electricsheep.zeroktube.request.video

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface VideoService {
    @GET("api/v1/videos/{id}")
    fun getVideo(@Path("id") vidId:Int):Call<VideoIdRequest>
}
