package xyz.electricsheep.zeroktube.request.comments

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface CommentService {
    @GET("api/v1/videos/{id}/comment-threads")
    fun listComments(@Path("id") vidId:Int):Call<DataComments>
    //fun listComments(): Call<DataComments> getComments(@Path("id") int comId)
}