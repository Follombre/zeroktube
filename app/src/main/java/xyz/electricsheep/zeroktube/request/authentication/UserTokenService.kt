package xyz.electricsheep.zeroktube.request.authentication

import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface UserTokenService {
    @FormUrlEncoded
    @POST("api/v1/users/token")
    fun fetchUserToken(@Field("client_id") client_id: String,
                       @Field("client_secret") client_secret: String,
                       @Field("grant_type") grant_type: String,
                       @Field("response_type") response_type: String,
                       @Field("username") username: String,
                       @Field("password") password: String): Call<UserToken>
}
