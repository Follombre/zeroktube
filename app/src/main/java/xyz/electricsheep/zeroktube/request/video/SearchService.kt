package xyz.electricsheep.zeroktube.request.video

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SearchService {
    @GET("api/v1/search/videos")
    fun search(@Query("search") query : String): Call<VideoListRequest>
}