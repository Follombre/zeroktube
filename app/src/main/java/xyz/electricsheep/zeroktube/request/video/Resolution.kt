package xyz.electricsheep.zeroktube.request.video

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "resolution")
data class Resolution(@PrimaryKey val label : String){
    constructor() : this("")
}