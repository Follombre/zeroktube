package xyz.electricsheep.zeroktube.request

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import xyz.electricsheep.zeroktube.BuildConfig
import xyz.electricsheep.zeroktube.request.authentication.ClientData
import xyz.electricsheep.zeroktube.request.authentication.ClientDataService
import xyz.electricsheep.zeroktube.videolist.AbstractTokenActivity
import xyz.electricsheep.zeroktube.request.authentication.UserToken
import xyz.electricsheep.zeroktube.request.authentication.UserTokenService
import xyz.electricsheep.zeroktube.videolist.AbstractVideoActivity
import xyz.electricsheep.zeroktube.request.comments.Comment
import xyz.electricsheep.zeroktube.request.comments.CommentService
import xyz.electricsheep.zeroktube.request.comments.DataComments
import xyz.electricsheep.zeroktube.request.video.*
import xyz.electricsheep.zeroktube.videolist.AbstractVideoListActivity
import xyz.electricsheep.zeroktube.request.video.SearchService

class RequestManager {
    private lateinit var comList: ArrayList<Comment>
    private lateinit var clientId: String
    private lateinit var clientSecret: String
    private val defaultHost: String = "framatube.org"

    //private  val pref = getSharedPreferences("prefUser",0)
    private lateinit var retrofit : Retrofit

    fun getVideo(id: Int, activity: AbstractVideoActivity) {
        val service = retrofit.create(VideoService::class.java)
        val videoRequest = service.getVideo(id)

        videoRequest.enqueue(object: Callback<VideoIdRequest> {
            override fun onResponse(call: Call<VideoIdRequest>, response: Response<VideoIdRequest>) {
                var vid = response.body()!!
                if(BuildConfig.DEBUG) {
                    Log.e("RequestManager", "getVideo onResponse => " + response.errorBody().toString())
                }
                getDesc(vid, activity)
            }

            override fun onFailure(call: Call<VideoIdRequest>, t: Throwable) {
                //  error("marche pas")
                if(BuildConfig.DEBUG) {
                    Log.e("RequestManager", "getVideo onFailure => ${t}")
                }
            }
        })
    }

    fun getDesc(videoIdRequest: VideoIdRequest, activity: AbstractVideoActivity){
        val descService = retrofit.create(DescriptionService::class.java)
        var description : Call<Description>
        description = descService.getDescription(videoIdRequest.uuid)
        description.enqueue(object : Callback<Description>{
            override fun onResponse(call : Call<Description>, response: Response<Description>){
                val desc = response.body()!!
                activity.onVideoLoaded(videoIdRequest, desc)

            }

            override fun onFailure(call: Call<Description>, t: Throwable) {
                if(BuildConfig.DEBUG) {
                    Log.e("RequestManager", "getDesc onFailure => ${t}")
                }
            }

        })
    }

    fun getVideos(activity: AbstractVideoListActivity) {
        getVideos(activity, (activity as AppCompatActivity).getSharedPreferences("host",0).getString("host",defaultHost))
    }

    fun getVideos(activity: AbstractVideoListActivity, url: String?) {
        getVideos(activity, url, "")
    }

    fun getVideos(activity: AbstractVideoListActivity, url: String?, path: String) {
        getVideos(activity,url,path,null)
    }

    fun getComments(id: Int) {
        val service = retrofit.create(CommentService::class.java)
        val commentRequest = service.listComments(id)

        commentRequest.enqueue(object : Callback<DataComments> {
            override fun onResponse(call: Call<DataComments>, response: Response<DataComments>) {
                val allComs = response.body()
                if (allComs != null)
                // comList = ArrayList<Comment>()
                comList = allComs.data as ArrayList<Comment>
                if(BuildConfig.DEBUG) {
                    Log.d("RequestManager", "getComments onResponse (TOTAL COM) => ${allComs!!.total}")
                    for (c in allComs.data)
                        Log.d("RequestManager","COMMENTAIRE : ${c.text}")
                }
            }

            override fun onFailure(call: Call<DataComments>, t: Throwable) {
                if(BuildConfig.DEBUG) {
                    Log.e("RequestManager", "getComments onFailure => $t")
                }
            }
        })
    }

    fun getVideos(activity: AbstractVideoListActivity, url: String?, path: String, userToken: String?) {
        retrofit = Retrofit.Builder()
            .baseUrl("https://" + url)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
        val service = retrofit.create(ListVideoService::class.java)
        val headers = HashMap<String,String>()
        if(BuildConfig.DEBUG) {
            Log.d("RequestManagerMarius", "getVideos user token: $userToken")
        }
        if(userToken != null && userToken != "") {
            if(BuildConfig.DEBUG) {
                Log.d("RequestManagerMarius", "getVideos: not empty")
            }
            headers["Authorization"] = "Bearer $userToken"
        }
        //TODO: add dynamic values as args, and loop it all
        val videoRequest = service.listVideos(path,headers,0,100)

        if(BuildConfig.DEBUG) {
            Log.d("RequestManagerMarius", "getVideos url => https://${url}/api/v1/${path}videos/")
        }
        videoRequest.enqueue(object : Callback<VideoListRequest>{
            override fun onResponse(call: Call<VideoListRequest>, response: Response<VideoListRequest>) {
                val allVid = response.body()
                if (allVid != null){
                    if(BuildConfig.DEBUG) {
                        //debug
                        Log.d("RequestManagerMarius", "getVideos marche")
                        Log.d("RequestManagerMarius", "getVideos => TOTAL: ${allVid.total}")
                        Log.d("RequestManagerMarius", "getVideos URL => $url")
                    }

                    for(vid in allVid.data) {
                        vid.thumbnailPath = "https://"+url+vid.thumbnailPath
                    }
                    activity.onVideoListLoaded(allVid.data)
                }
                else {
                    if(BuildConfig.DEBUG) {
                        Log.e("RequestManagerJules", "getVideos")
                    }
                }
            }

            override fun onFailure(call: Call<VideoListRequest>, t: Throwable) {
                if(BuildConfig.DEBUG) {
                    //  error("marche pas")
                    Log.e("RequestManagerMarius", "GetVideos error : $t")
                }
            }
        })
    }

    fun fetchUserToken(activity: AbstractTokenActivity, username: String, password: String) {
        val response_type = "code"
        val grant_type = "password"

        val service = retrofit.create(UserTokenService::class.java)
        val request = service.fetchUserToken(clientId,clientSecret,grant_type,response_type,username,password)

        request.enqueue(object : Callback<UserToken> {
            override fun onResponse(call: Call<UserToken>, response: Response<UserToken>) {
                val userToken = response.body()

                if (userToken != null) {
                    (activity as AppCompatActivity).getSharedPreferences("userToken",0).edit()
                        .putString("access_token",userToken.access_token)
                        .apply()
                    activity.onTokenLoaded(userToken)
                }
                else {
                    if(BuildConfig.DEBUG) {
                        Log.e("RequestManagerMarius", "fetchUserToken onResponse => Token vide")
                    }
                }
            }

            override fun onFailure(call: Call<UserToken>, t: Throwable) {
                if(BuildConfig.DEBUG) {
                    Log.e("RequestManagerMarius", "fetchUserToken onFailure => ERREUR TOKEN: $t")
                }
            }
        })
    }

    fun getSubscriptionFeed(activity: AbstractVideoListActivity) {
        if(BuildConfig.DEBUG) {
            Log.d("RequestManagerMarius", "getSubscriptionFeed")
        }
        val path = "users/me/subscriptions/"
        if(BuildConfig.DEBUG) {
            Log.d("RequestManagerMarius",
                "getSubscriptionFeed => ${(activity as AppCompatActivity).getSharedPreferences("userToken", 0)
                    .getString("access_token", null)} est le token"
            )
        }
        getVideos(activity, (activity as AppCompatActivity).getSharedPreferences("host",0).getString("host",defaultHost), path, (activity as AppCompatActivity).getSharedPreferences("userToken",0).getString("access_token",null))
    }

    fun search(query : String, activity: AbstractVideoListActivity){
        search(query, activity, (activity as AppCompatActivity).getSharedPreferences("host",0).getString("host",defaultHost))
    }

    fun search(query : String, activity: AbstractVideoListActivity, newHost : String?){
        val service = retrofit.create(SearchService::class.java)
        val searchRequest = service.search(query)

        searchRequest.enqueue(object  : Callback<VideoListRequest>{
            override fun onResponse(call: Call<VideoListRequest>,response: Response<VideoListRequest>) {
                val res = response.body()
                if (res != null){
                    for(vid in res.data) {
                        vid.thumbnailPath = "https://"+newHost+vid.thumbnailPath
                    }

                    activity.onVideoListLoaded(res.data)
                }
            }

            override fun onFailure(call: Call<VideoListRequest>, t: Throwable) {
                if(BuildConfig.DEBUG) {
                    Log.e("RequestManager", "search onFailure => ${t}")
                }
            }
        })
    }

    fun setHost (activity: AbstractSetHostActivity, url : String){
        (activity as AppCompatActivity).getSharedPreferences("host",0).edit()
            .putString("host",url)
            .apply()
        if(BuildConfig.DEBUG) {
            Log.d("RequestManagerMarius", "setHost => urls : $url")
        }
        retrofit = Retrofit.Builder()
            .baseUrl("https://"+url)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
        fetchClientData(activity)
    }

    private fun fetchClientData(activity: AbstractSetHostActivity) {
        val service = retrofit.create(ClientDataService::class.java)
        val request = service.getData()
        request.enqueue(object: Callback<ClientData> {
            override fun onResponse(call: Call<ClientData>, response: Response<ClientData>) {
                val client = response.body()
                clientId = client?.client_id.toString()
                clientSecret = client?.client_secret.toString()
                activity.onHostSet()
            }

            override fun onFailure(call: Call<ClientData>, t: Throwable) {
                if(BuildConfig.DEBUG) {
                    Log.e("RequestManagerMarius", "fetchClientData onFailure => $t : ERREUR CLIENT DATA")
                }
            }
        })
    }

}