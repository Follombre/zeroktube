package xyz.electricsheep.zeroktube.request.video

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "files")
data class VideoFiles (@PrimaryKey (autoGenerate = true) var id : Long,
                       var fileUrl: String,
                       var fileDownloadUrl : String,
                       @Embedded var resolution: Resolution,
                       var videoId : Int)
{
    constructor(resolution: Resolution) : this (-1, "", "",resolution, -1)
}