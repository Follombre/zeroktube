package xyz.electricsheep.zeroktube.request.comments

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "comment")
data class Comment(@PrimaryKey(autoGenerate = true) var id: Long,
                   var text: String,
                   var videoId: Int)
{
    constructor() : this(-1, "", -1)
}