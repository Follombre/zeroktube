package xyz.electricsheep.zeroktube.videolist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.request.comments.Comment


/**
 *
 */

class CommentListAdapter(private val commentSet: ArrayList<Comment>) :
    RecyclerView.Adapter<CommentListAdapter.CommentListViewHolder>() {

    /** Provides a reference to the views for each data item */
    class CommentListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

       // val user: TextView = itemView.findViewById(R.id.user)
        val content: TextView = itemView.findViewById(R.id.content)

        /**
         * Binds a [Comment] to a [RecyclerView] item
         */

        fun bindComment(comment: Comment) {
          //  user.text = toString(comment.user)
            content.text = comment.text
        }
    }

    /** Creates new views (invoked by the layout manager (?)) */
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CommentListViewHolder {

        /** creates a new view */
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.commentlist_item, parent, false)
        return CommentListViewHolder(view)
    }

    /** Replaces the content of a view */
    override fun onBindViewHolder(holder: CommentListViewHolder, position: Int) {
        /**
         * gets element from videoSet at this position and
         * replaces the content of the view with that element
         */
        holder.bindComment(commentSet[position])
    }

    /** Returns the size of [commentSet] (invoked by the layout manager (?)) */
    override fun getItemCount() = commentSet.size

}