package xyz.electricsheep.zeroktube.commentslist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import xyz.electricsheep.zeroktube.R
import xyz.electricsheep.zeroktube.request.comments.Comment
//import xyz.electricsheep.zeroktube.videoPlayer.VideoMedia
import xyz.electricsheep.zeroktube.videolist.CommentListAdapter


class CommentListActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<CommentListAdapter.CommentListViewHolder>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.commentlist)

        viewManager = LinearLayoutManager(this)

        //dummy comments for testing purposes

        //viewAdapter = CommentListAdapter(commentSet = arrayListOf(Comment("trop bien"),Comment("trop nul")))

        //initializes the recyclerView
        recyclerView = findViewById<RecyclerView>(R.id.recyclerComments).apply {

            //improve performance if the content doesn't change the size of the RecyclerView layout element
            setHasFixedSize(true)

            layoutManager = viewManager
            adapter = viewAdapter

        }
    }

    private fun commentListItemHandler(comment: Comment) {
        startActivity(Intent(this,Comment::class.java))
    }
}
