# To do list of Android PeerTube Client

## Interface
- [ ] Choose between a light/dark theme
- [ ] Choose the main color
  - [ ] Orange by default
- [ ] Choose the color of the notification led
  - [ ] Orange by default
- [ ] Choose the language
  - [ ] French
  - [ ] English
  - [ ] Collaborative platform (http://zanata.org | https://weblate.org | others)


## Features & functions
- [ ] Watch videos
  - [ ] Like or dislike
  - [ ] Share
  - [ ] Save to a playlist (need to be connected)
  - [ ] Download
  - [ ] Report
  - [ ] Add/Modify a comment
  - [ ] Keep the application in the background
- [ ] Subscribe/Unsubscribe to channels
- [ ] Choose the instance
- [ ] Set up your account (need to be connected)
- [ ] Manage your channels and videos (need to be connected)
- [ ] Consult subscription feed (connected)
- [ ] Play a playlist from the library (beginning at a selected video)
- [ ] Search for a channel/user
- [ ] Search for a video (by title/owner/upload date/URL)
- [ ] Consult popular videos
- [ ] Notify (new video, new comment, video takedown)
- [ ] Consult notifications
